<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'augusto_iimoveis' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'YTQsG>tOWcuVPYD)Go|KM_f[GG`Hb1~Q:(*F},BI@_IZxi-vIaAu_N-/eyLz@)}q' );
define( 'SECURE_AUTH_KEY',  ')d`vQ_Fb2VC8:E#Rqpj|U:8YpO&W+n(j+H~}IkK88]MR`dd~`F9F`w_AxPdssS,#' );
define( 'LOGGED_IN_KEY',    'u_<9oJc`c{$@mz6WypaTZ:}1Pl TB!c2vI~j6MTASv!]&rF~Kpbllyk2hiMB>?bQ' );
define( 'NONCE_KEY',        '~-#}pdf5j>|Y05Df!xrh*8zK;&<bN $p@G:8S_(${8OtneaQQqtoyVNT|S]ry`Vy' );
define( 'AUTH_SALT',        'O@G)8>I0@p ySNmj-Oi.UnKEsYZ{$!7e/s{&H Y W5~&9_!CJu^YT&PED3Fll0jB' );
define( 'SECURE_AUTH_SALT', '4EAVHkuni7J}_6GWUHK7l&/,(b^Iq^DQQVK8FoQ rog2_^SJsyCx)8F`t8,)YZ9p' );
define( 'LOGGED_IN_SALT',   'Ro,NkdXPbc{ {g3y-Lke;Zk7_: ~3ip:@BLvoGB?XmnrO]YOP6a8M;3+WjfC^vO.' );
define( 'NONCE_SALT',       'L+`J@x2b253alW.lK{~SYy}3#%9Jg5?) $KH;KK{v%hDv/`FOmI@i$/!6EZ~tnC{' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
